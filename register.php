<!DOCTYPE html>
<html lang='en'>

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Signin</title>
    <link rel='stylesheet' type='text/css' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' />
    <link rel='stylesheet' type='text/css' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css' />
    <link rel='stylesheet' type='text/css' href='register.css'>

</head>
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js'></script>

<body>
    <form method='post' action='register.php' enctype="multipart/form-data">
        <?php
        $faculty = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
        $gender = array(0 => 'Nam', 1 => 'Nữ');
        $msg = array();
        $msg["name"] = "<br>";
        $msg["gender"] = "<br>";
        $msg["faculty"] = "<br>";
        $msg["dateBirth"] = "<br>";
        if (isset($_POST['submit'])) {
            session_start();
            $_SESSION = $_POST;
            $check = true;
            if (empty($_POST["name"])) {
                $msg["name"] = "<span class='error'>Hãy nhập tên.</span><br>";
                $check = false;
            }
            if (!isset($_POST["gender"])) {
                $msg["gender"] = "<span class='error'>Hãy chọn giới tính.</span><br>";
                $check = false;
            } else {
                $_SESSION["gender"] = $gender[$_POST["gender"]];
            }
            if (empty($_POST["faculty"])) {
                $msg["faculty"] = "<span class='error'>Hãy chọn phân khoa.</span><br>";
                $check = false;
            } else {
                $_SESSION["faculty"] = $faculty[$_POST["faculty"]];
            }
            if (empty($_POST["dateBirth"])) {
                $msg["dateBirth"] = "<span class='error'>Hãy nhập ngày sinh.</span><br>";
                $check = false;
            }
            $filepath = "images/" . basename($_FILES["file"]["name"]);

            if (!file_exists($_FILES['file']['tmp_name']) || !is_uploaded_file($_FILES['file']['tmp_name'])) {
                $check = false;
            } else if (move_uploaded_file($_FILES["file"]["tmp_name"], $filepath)) {
                $_SESSION["image"] = $filepath;
            } else {
                $check = false;
            }

            if ($check) {
                header('location: confirm.php');
            }


            // if () {
            //     echo "<img src=" . $filepath . " height=200 width=300 />";
            // }
        }
        echo $msg["name"];
        echo $msg["gender"];
        echo $msg["faculty"];
        echo $msg["dateBirth"]; ?>
        <table>
            <tr>
                <td class='green_background'>
                    <label class='required'> Họ và tên </label>
                </td>
                <td>
                    <input type='text' class='frames' style=' width: 240px;' name='name' value='<?php echo isset($_POST['name']) ? $_POST['name'] : ''; ?>'>
                </td>
            </tr>

            <tr>
                <td class='green_background'>
                    <label class='required'> Giới tính </label>
                </td>
                <td>
                    <?php
                    for ($i = 0; $i < count($gender); $i++) {
                        echo "<input type='radio'  name='gender' style='margin: 5px' value='" . $i . "'";
                        echo isset($_POST['gender']) && $_POST['gender'] == $i ? " checked" : "";
                        echo " /> " . $gender[$i];
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td class='green_background'>
                    <label class='required'> Phân Khoa </label>
                </td>
                <td>
                    <select class='blue-box' name='faculty'>
                        <?php
                        foreach ($faculty as $key => $value) {
                            echo " <option ";
                            echo isset($_POST['faculty']) && $_POST['faculty'] == $key ? "selected " : "";
                            echo "value ='" . $key . "'>" . $value . "</option> ";
                        }
                        ?>
                </td>
            </tr>
            <tr>
                <td class='green_background'>
                    <label class='required'> Ngày sinh </label>
                </td>
                <td> <input type='text' onkeydown='return false' class='date form-control blue-box' name='dateBirth' placeholder='dd/mm/yyyy' value='<?php echo isset($_POST['dateBirth']) ? $_POST['dateBirth'] : ""; ?>'>
                    <script type='text/javascript'>
                        $('.date').datepicker({
                            format: 'dd/mm/yyyy',
                        });
                    </script>
                </td>
            </tr>

            <tr>
                <td class='green_background'> <label> Địa chỉ </label> </td>
                <td>
                    <input type='text' class='frames' style=' width: 240px; ' name='address' value='<?php echo isset($_POST['address']) ? $_POST['address'] : ""; ?>'>
                </td>
            </tr>
            <tr>
                <td class='green_background'> <label> Hình ảnh </label> </td>
                <td>
                    <input type="file" height="30px" id="image_upload" name="file" accept="image/*" id="file">
                </td>
            </tr>
        </table>
        <input type='submit' name='submit' value='Đăng kí' class='submit'>
    </form>

</body>

</html>